---
title: Radio France International Mandenkan
date:  2020-05-08T14:07:30
type: notification 
tags: [ "jula", "language"]
categories: ["Julakan" ]
image: /images/rfi_mandenkan.jpg
hyperlink: https://www.rfi.fr/ma
---

= Radio France International Mandenkan
:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang: fr 
include::locale/attributes.adoc[fr]

Ecoutez l’actualité internationale, africaine, française et régionale
(Afrique de l’Ouest) en mandenkan, ainsi que le sport, une revue de presse régionale.
Basé à Bambako, mais diffusé aux locuteurs mandenkan en Afrique de l'ouest. Mise-à-jour quotidiennement  
Les infos en 10 minutes, suivi par une émission d'actualité de 20 minutes du lundi au vendredi à 8h00 et 12h00.
