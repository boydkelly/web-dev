---
title:  "a sen b'a la"
date:  2018-08-07T07:05:01+00:00
type: post
---

= **a sen b'a la**
:author: Boyd Kelly
:date: 2018-08-07T07:05:01+00:00
:type: post
:description: A sen b'a la
:lang: fr 
:tags: ["jula language"]
:topics: ["julakan"]
include::./locale/attributes.adoc[fr]

[cols="2"]
|===
| Il est impliqué.
| (Il a son pied dedans.)
|===
