---
title: Radio France International Mandenkan
date:  2020-05-08T14:07:30
type: notification 
tags: [ "jula", "language"]
categories: ["Julakan" ]
image: /images/rfi_mandenkan.jpg
hyperlink: https://www.rfi.fr/ma
---

= Radio France International Mandenkan
:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang:  en 
include::locale/attributes.adoc[fr]

Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.
Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.
10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.

