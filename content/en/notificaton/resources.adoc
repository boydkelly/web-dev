
=== The link:http://www.mandenkan.com[Mandenkan.com] web site
This web site is produced by a true linuguistic expert, who also offers personal tutoring in Abidjan and distance learning.

image::logo-mandenkan.jpg[http://www.mandenkan.com]

=== Radio France International Mandenkan

Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.
Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.

link:https://www.ma.rfi.fr[rfi Mandenkan]

=== An ka taa

An expert linguist who provides online courses as well an awesome Youtube channel which teaches basic Jula and Bambara, quite suitable for Jula learniers in Ivory Coast.

link:https://www.ankataa.com[ankataa]

=== Radio Media+ Bouaké

=== La Bible

link:https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US[Bible en Jula avec audio et français]
This application with no advertising provides parallel texts of the Bible in French and Jula.  (Sorry Anglophones!)

===  Les site link:http://www.jw.org/dyu[jw.org en Jula] et link:http://www.jw.org/bm[jw.org en Bambara]

Ces sites contiennent des articles, des vidéos et pistes mp3 en Jula de Burkina Faso à comparer avec les mêmes articles en bambara, français, ou en anglais.
These sites provide written material, videos and audio files in Burkina Faso Jula, Bambara, English, and French.  

