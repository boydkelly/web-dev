---
title:  Jula vocabulary for Anki
date:  2018-05-25T13:07:27Z
type: post
tags: ["jula", "language", "anki"]
categories: ["Julakan"]
image: /images/anki2.jpeg
---

= Jula vocabulary for Anki
:author: Boyd Kelly
:date: 2018-05-25T13:07:27Z
:description: Jula vocabulary for Anki
:imagedir: /images/
:lang: en
:type: post

image::anki2.jpeg[role="left"] Its been a while since I have updated my Jula.apkg file for public download. 
I do realize that flash cards to learn Jula are not the hottest thing right now, but it is a bit of a shame that the link:ankiweb.net people[[ankiweb.net\]] will delete so-called less popular downloads.

Anyways for this reason, I will share my files on Google Drive and send notifications on Social Media from time to time.
I have made substantial updates to both the content and presentation. 
Many cards are now defined to show either regional or alternative variations of words. 
A next step will be to convert some of the existing cards to the newer format.

For anyone not familiar with link:Anki[[ankiweb.net\]], according to wikipedia, Anki is a spaced repetition flashcard program. 
"Anki" is the Japanese word for "memorization".
The SM2 algorithm, created for SuperMemo in the late 1980s, forms the basis of the spaced repetition methods employed in the program.

Anki is open source and is available on most platforms that you can think of: Android, IOS, Mac, Linux, Windows, Web. 
It's a great too for memorizing anything.

If you would like to use it to learn Jula, please feel free to link:http://bit.ly/2sb0jMw[download my file.]

After that link:https://apps.ankiweb.net/#download[download the application version] for you platform of choice. You can also search for Anki on Google Play or the App Store. Android users are advantaged here is the IOS app is the only paid version. (No comment LOL).

Once you have the app installed, on Mac or Windows, open Anki and go to File, Import. 
You should be good to go. 
On Android, if you tap on the apkg file from within your file manager it should prompt you to open with Anki. 
I would choose "Always", as no other program that I know of is using that extension. Enjoy and any suggestions or contributions are welcome.

link:contact.adoc[Contact me]
