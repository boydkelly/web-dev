---
title: "If the shoe fits..."
date: 2020-06-06T11:11:52Z
author: unknown
draft: false
lang: en 
type: false
tags: ["proverb", "english"]
categories: ["French in English"]
---

= If the shoe fits, wear it. 

[quote]
____
If the shoe fits, wear it.
____
