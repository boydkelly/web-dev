---
title:  Easy French Punctuation on Linux with Nvim/Vim 
date:  2020-05-07T16:31:34
type: post
tags: ["tech", "vim", "french", "language"]
categories: ["Technology"]
image: /images/vim.png
---

= Easy French Punctuation on Linux with Nvim/Vim 
:author: Boyd Kelly
:email:
:description: Easy French Punctuation
:date: 2020-05-07T16:31:34
:filename: ponctuation-francaise.adoc
:imagesdir: /images/
:type: post
:keywords: [vim, nvim, technolgie, technology, Africa, afrique, linux, teamubuntu, fedora]
:experimental:
:lang: en 

[quote,King Solomon,Proverbs 15:23 TMN]
____

A word at its right time is O how good!
____

image::vim.png[]

If you like me type a lot in both English and French, how do you deal with the spaces before question marks, exclamation marks colons, semi-colons, as well as quote marks.  In French these spaces are mandatory although obnoxious for some English speakers.

With my preferred editor Nvim on Linux this is easy.

* Create a file at the following location for either Vim or Nvim:

[cols=2]
|====
|(vim/gvim): 
|*.vim/keymap/fr.vim)*
|(nvim): 
|*.local/share/nvim/site/keymap/fr.vim*
|====

* With the following contents:

....
let b:keymap_name="French"

"highlight lCursor ctermbg=red guibg=red

loadkeymap

?   ?
!   !
:   :
;   ;
....

[NOTE]
====
The spaces before the punctuation marks are nonbreakable spaces.
These unicode characters can be entered in nvim/vim by pressing kbd:[Ctrl + V] kbd:[u] kbd:[0] kbd:[0] kbd:[a] kbd:[0] and normally you should also be able to cut and paste the text above.  
====

This takes care of the question mark, exclamation mark, colon and semi colon.
The quote marks need a bit more work.
For those lets create a specific "french.vim" configuration file which will also load the preceding keymap in:

[cols="2"]
|====
|(vim/gim):
|~/.vim/keymap/french.vim
|(nvim):
|~/.config/nvim/french.vim
|====

* With the following content:

....
"keymappings unique to french
set spelllang=fr
set keymap=french
:imap <expr> " getline('.')[col('.')-2]=~'\S' ?  ' »' : '« '
":imap <expr> ' getline('.')[col('.')-2]=~'\S' ?  ' »' : '« '
set background=dark
set wordwrap
....

This preceding file now takes care of loading the keymap, and also replacing the English " with either « or » depending on the position and also including the non breaking space.

This file for me also makes a change to the colorscheme when I'm editing a file of a particular language, and also sets the wordrap option.
(More about that another time and space!)

Now there are several ways to load this.
One simple way is to trigger this by the spelllang option.
You can accomplish this by putting the following in your .vimrc or .config/nvim/.init.vim:

....
autocmd OptionSet spelllang call LanguageSettings()

function! LanguageSettings()
  if &spelllang =~ 'fr'
		source ~/.config/nvim/fr.vim
	elseif &spelllang =~ 'en'
		source ~/.config/nvim/en.vim
  endif
endfuction
....

Now if you change the spelling dictionary to French, the keyboard configuration options will be activated automatically!

== Try it!

