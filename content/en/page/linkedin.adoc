---
title:  Boyd Kelly on Linkedin
date:  2018-05-05T20:43:32Z
type: page
---

= Boyd Kelly on Linkedin
:categories: ["about"]
:date: 2018-05-05T20:43:32Z
:draft: false
:lang: en
:link: https://www.linkedin.com/in/boydkelly
:type: link

Contact me via linkedin or the contact form below
