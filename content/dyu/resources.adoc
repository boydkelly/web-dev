---
title:  Quelques resources pour les apprenants de Jula en Côte d'Ivoire 
date:  2020-05-08T14:07:30
type: home
draft: true
tags: [ "jula", "language"]
categories: ["Julakan" ]
---

= Quelques resources pour les apprenants de Jula en Côte d'Ivoire 
:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: draft
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang: fr 
include::locale/attributes.adoc[fr]

image::image.png[]

== Ressources pour les apprenants de Julakan ivoirien

=== Le site web link:http://www.mandenkan.com[Mandenkan.com]
Ce site appartient à un véritable expert linguistique qui offre des cours particuliers à Abidjan, mais aussi des cours à distance. 

image::logo-mandenkan.jpg[http://www.mandenkan.com]

=== Radio France International Mandenkan
Ecoutez l’actualité internationale, africaine, française et régionale
(Afrique de l’Ouest) en mandenkan, ainsi que le sport, une revue de presse régionale.
Basé à Bambako, mais diffusé aux locuteurs mandenkan en Afrique de l'ouest. Mise-à-jour quotidiennement  

=== An ka taa
Vidéos, dictionnaires, cours par un expert qui a passé des années à étudier les langues mandingue au Burkina Faso et Mali

link:https://www.ankataa.com[ankataa]

=== Radio Media+ Bouaké

=== La Bible
link:https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US[Bible en Jula avec audio et français]
Comparer des textes traduits en Jula de Côte d'Ivoire avec la version Louis Ségond.

===  Le site link:http://www.jw.org/dyu[jw.org en Jula] et link:http://www.jw.org/bm[jw.org en Bambara]
Ces sites contiennent des articles, des vidéos et pistes audio en Jula de Burkina Faso à comparer avec les mêmes articles en bambara, français, ou en anglais.

