---
title: Ngalonci
date: 2020-06-18
author: mɔgɔ do
draft: false
type: home 
lang: dyu
tags: ["julakan", "ngalonci"]
categories: ["tarikuw"]
image: /images/DSC00932.JPG
---

= Ngalonci
:author: inconnu
:date: 2020-06-18
:type: post
:lang: dyu
:imagesdir: /images/
:skip-front-matter:

image:DSC00932.JPG[role="left"]

Cɛ dɔ tun bɛ dugu dɔ la, o cɛ tɔgɔ tun kiyanin kojugu. A tolo don na masacɛ dɔ ya nafolotigiya la. A ka a fɔ a muso nyana ko a ye a torolasanu d’a ma. A facɛ fana sa la ka so kelen to a bolo. A ka fɔ a a muso nyana ko a bɛ taga masacɛ fɛ ka taga a ya masaya bɔsi a la. A ka nyɔ kɛ so kun ganfa la ka sanukuru bla o la. So ka nyɔ ni sanu kunu. A sɔrɔ la ka taga fɔ masacɛ nyana ko so dɔ bɛ ale bolo. So bo ye sanu ye. So ka bo kɛ, a ka bo nin ko ka sanu ye a la.



O kɛnin, masacɛ ka so san sɔngɔ gbɛlɛn na. A ka so bla kuru dɔ kɔnɔ, ni so ka bo min kɛ, o bɛ o cɛ fɔ so nan na bo caman kɛ. O ka sɔrɔ cɛ se la yɔrɔ jan. Masacɛ ka a lase jɔnmuso bɛɛ ma ko a ya so boko ye bi ye. Musow nan na ni fiyɛw ye ka nan bo bɛɛ ko sanukuru kelen ne sɔrɔ la a la. Masacɛ ko: « A ye taga cɛ nin wele ka nan n b’a kan tigɛ ». Cɛ nantɔ, a ni a muso bɛn na a la ko ni a ni masacɛ ka kuman daminɛ, i bɛ n sɔsɔ ka n galontigiya. O tuman na a ka sisɛ faga ka sisɛ joli kɛ sisɛjogi kɔnɔ ka a siri a muso kankɔrɔ.



O senin masacɛ fɛ, masacɛ ko: « Ile ka so min feere n ma ko a bo ye sanu ye, sanu wɛrɛ ma sɔrɔ a kɔnɔ fɔ sanukurukelen.» Cɛ muso ko: « Eh! Ile tɛ ne cɛ nin lɔn, namaratɔba, a nyɔgɔn ngalontigɛla tɛ jamana nin kɔnɔ. » Cɛ wili la a muso kama ko: « O tuman na i bɛ ne dɔgɔya wa? » A sin na ka a muso ta ka a pɛrɛn dugu ma ka muru kɛ ka jogi ci ka joli seri fan bɛɛ fɛ. bɛɛ ko: « A ka muso faga, a ka muso faga! »



A ko masacɛ ma: « An ye tɛmɛ ni an ya kuman ye! » Masacɛ ko: « Ka mɔgɔ faga ko an ye tɛmɛ ni kuman ye, o tɛ se ka kɛ. » Cɛ ko: « A bɛna wili sisan, a ye nan ni fiyɛ kula ni kɔlɔnnaji ani flannan ye. » A ka kilisi fɔ kɔlɔnnaji la ka flannan bla o kɔnɔ. A bannin kɔ ola, a ka ji seriseri muso kan ni flannan ye. Muso tusho la ka wili. Bɛɛ dabali ban na. Masacɛ ka a nyini cɛ nin fɛ ko a ye a kalan nin lɔnin na. Ko ale bɛ nin dalilu fɛ. Cɛ ko masacɛ ma ko: « Fla nin tɛ sɔrɔ ten dɛ! » A ka waridaba dɔ fɔ. Masacɛ ka o di a ma tugu. Fla kɛcogo fɔ la masacɛ nyana minkɛ, masacɛ ko a bɛ fla kɛ ka flɛ. Masacɛ ka a musofitinin, kɔnyɔnkula wele ka a nyafɔ a ye.



A to la baro la o lon a muso ka a sɔsɔ. A wili la ka a muso kan tigɛ ni muru dadiyanin ye. Bɛɛ jatigɛ la. A ko : « Nin ye ko denin ne ye », a fana ka fiyɛ, kɔlɔnnaji ani flannan  ta ka ya kilisiw fɔ. A ka ji seri muso kan ka dɛsɛ muso ma wili. A ka ko bɛɛ kɛ muso ma wili. Masacɛ dimi na fo a bɛ yɛlɛyɛlɛ. A ko: « A ye taga ale mɔgɔlankolon mina ka


nan ni a ye sisansisan ma kan tigɛ. » O taga la cɛ minan ka nan. Siyan nin na, bɛɛ kɔni la la a la ko cɛ kan bɛ tigɛ.



Masacɛ ka misitura, san woronfila tura le kan tigɛ ka o gbolo bɔ a la. O ka galontigɛla bla gbolo nin kɔnɔ ka a karan a da la.



O b’a fɛ ka a fili baji kɔnɔ. O se la bada la minkɛ, mankanba dɔ le wili la so kɔnɔ. O ka ale jigi ka taga o konyanw nyanabɔ sabu olugu ye masacɛ ya kɛlɛdenw ye. Fɔlɔfɔlɔ, duguw tun bɛ ben nyɔgɔn kan tuman caman. A bɛ kɛlɛdenw nyana o nyɔgɔn na kɛlɛ dɔ le bɛ. O taganin kɔ, jagokɛla dɔ timitɔ tagamakan don na galontigɛla tolo la.



A sin na ka kule daminɛ ni nin kan ye : « N tɛ sanu fɛ hali ni i ka n jagboya n tɛ i ya sanu fɛ. Ne sanu, n t’a fɛ. N tɛ sanu fɛ fewu ». Cɛ nan na ka nan gbolo lagbɛ. A ko : « Mɔgɔ le bɛ nin kɔnɔ wa ? » cɛ bɛ ale sanu kuman kelen le fɔ la. A ko : « Ile bɛ mun ne sanuko kuman na hali bi ? ». Galontigɛla ko a ma ko gbolo fanin bɛ sanu na nka o ko ale ma ko a ye tele saba kɛ gbolo kɔnɔ kasɔrɔ ka sanu ta. « N ko n tɛ sanu fɛ. Ile bɛ sanu fɛ wa ? »



Jagokɛla ko : « ɔn-hɔn ne b’a fɛ ». « Ni i kɔnɔ bɛ sanu fɛ i bɛ se ka ne labɔ nka i bɛ to gbolo kɔnɔ tele saba dɛ! » Jagokɛla sɔn na, a ko : « Ile ma fɔ sanu wa ? Hali ni i ko lɔgɔkun kelen n sɔn na o ma. » A ka jagokɛla bla gbolo kɔnɔ ka cɛ ya fali ni a ya minanw bɛɛ ta ka taga. Kɛlɛdenw segi la minkɛ, o nan na gbolo sɔrɔ bada la. O ko o bɛ gbolo fili ji la wagati min, cɛ ko: « Ne tɛ, cɛ  min tun bɛ gbolo kɔnɔ o taga la kaban. Ne ye timinbagatɔ le ye ». Kɛlɛdenw ko : « Jɔn bɛ ile ya mananmanankanw lamɛn, namaratɔ ». O taga la cɛ fili ba cɛman.



Tele damandaman timi na minkɛ, namaratɔ nan na bɔ masacɛ ye. A ko :

« Laharakaw bɛ i fo, tɔrɔ si tɛ o la. I mɔncɛ b’i fo. I nyanafin bɛ o la kojugu. Lahara ka di. Ne yɛrɛ bɛna segi o! dununya ye nyagban le ye. Baara tɛ yen, foyi foyi tɛ yen ni lagafiya tɛ. Min ka di i ye i bɛ ole kɛ. Lagafiya dan ne bɛ. Muso tɛ i nyagban, den tɛ i nyagban, foyi tɛ i nyagban. » Masacɛ la kojugu cɛ ya kuman na, a ko: « Kɔmi ile ko ko n nyanafinba bɛ n mamacɛ ni n mamamusow la, ne yɛrɛ bɛna taga lahara. » O ka san woronfila tura dɔ faga ka masacɛ bla o gbolo kɔnɔ ka taga a fili baji la. Cɛ nan na sigi masaya la sabu bɛ sinan na a nya a ya daliluw kosɔn. Hali bi masacɛ ma segi, cɛ le bɛ masaya la.



N k’o sɔrɔ yɔrɔ min n k’o bla yen
