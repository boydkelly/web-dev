#!/usr/bin/bash
[[ -x /usr/bin/podman ]] || { echo "Run outside of toolbox!" ; exit 1 ; } 
export AUTH=cSXqNYC54yL3y1CRmgjd
echo $AUTH
#sed s/\$AUTH/"$AUTH"/g local.yml > tmp.yml

podman run --rm -it \
  --name asciidoctor-hugo \
  --security-opt label=disable \
  -v `pwd`:/site:z asciidoctor-hugo bash 
